
# NB. Working directory is dist

composer archive create --sourceType dir --sourceName ../

composer network install --archiveFile airlinev9@0.0.1.bna --card PeerAdmin@hlfv1

composer network start --networkName airlinev9 --networkVersion 0.0.1 --networkAdmin admin --networkAdminEnrollSecret adminpw --card PeerAdmin@hlfv1

# composer network upgrade --networkName airlinev9 --networkVersion 0.0.1 --card PeerAdmin@hlfv1

composer card import --file admin@airlinev9.card

composer network ping --card admin@airlinev9

composer-rest-server --card admin@airlinev9