# NB. Workind dir is dist
composer participant add --card admin@airlinev9 --data '{"$class":"org.acme.airline.participant.ACMENetworkAdmin","participantKey":"johnd","contact":{"$class":"org.acme.airline.participant.Contact","fName":"John","lname":"Doe","email":"john.doe@acmeairline.com"}}'
composer participant add --card admin@airlinev9 --data '{"$class":"org.acme.airline.participant.ACMEPersonnel","participantKey":"personnel1","contact":{"$class":"org.acme.airline.participant.Contact","fName":"personnel","lname":"One","email":"personnel.one@acmeairline.com"}, "department":"Logistics"}'

composer identity issue --newUserId johnd --participantId org.acme.airline.participant.ACMENetworkAdmin#johnd --card admin@airlinev9 -x
composer identity issue --newUserId personnel1 --participantId org.acme.airline.participant.ACMEPersonnel#personnel1 --card admin@airlinev9

composer card import --file johnd@airlinev9.card

# If permissions.acl was updated it needs to be redeployed
#  composer network upgrade --networkName airlinev9 --networkVersion 0.0.1 --card PeerAdmin@hlfv1
